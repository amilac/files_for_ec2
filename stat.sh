#!/bin/bash

MemTotal=$(awk '/MemFree/ { printf "%.3f \n", $2/1024/1024 }' /proc/meminfo )
MemAvailable=$(awk '/MemAvailable/ { printf "%.3f \n", $2/1024/1024 }' /proc/meminfo )
uptime=$(uptime | awk -F'( |,|:)+' '{print $6,$7",",$8,"hours,",$9,"minutes."}')
HDD=$(df -h | grep /dev/mapper/centos-root)
CPU=$(top -bn2 | grep '%Cpu' | tail -1 | grep -P '(....|...) id,'|awk '{print 100-$8 "%"}')
dateTime=$(date +'%Y-%m-%d : %H-%M')


cat >>/home/ec2-user/stat.json <<EOF
    {
      {
	"dateTime":"$dateTime",
         "CPU":"$CPU",
          "MemTotal":"$MemTotal",
          "MemAvailable":"$MemAvailable",
	   "uptime":"$uptime",
	   "HDD":"$HDD"
      }
    }
EOF
